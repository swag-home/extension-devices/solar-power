# ☀ Solar Power Board

Documentation of the solar power board to store and supply power from a solar panel to a device

![3D Render](https://gitlab.com/swag-home/extension-devices/solar-power/-/raw/main/images/solar_3d_view.PNG)

## Goals of this project

Provide an accessible board to power device from a solar panel with a regulated output.

The board will output by default 3.3V but and the controller should be programmable as device through the I2C bus and also from its Swagger API if the controller possess a WiFi interface.

The board is designed as an asynchronous step-down regulator to reduce the complexity and maintenance issues of a synchronous step-down regulator at the cost of a slightly reduced efficiency while still having a much better efficiency than a linear a regulator and not requiring to depends on a specific IC

## Description of the board

The schematic of the board is available here:

![Schematic](https://gitlab.com/swag-home/extension-devices/solar-power/-/raw/main/schematics/Schematic_SolarPowerBoard.svg)

The board is composed of the following sections shown on the schematic:

- `DC1`: Connector to rely the solar panel to the board
- `DC2`: Connector on which the regulated output is exposed
- `H2`: Connector for the controller which will act as DC/DC converter (and also an MPPT tracker ) to regulate
- `H1`: Connector for the I2C interface of the controller to optionally program the output voltage and track power input / output
- `H3`: Connector to switch on/off or reset the controller. If nothing uses them the controller is always on by default if some power is present

## Preview of the board

Simulated renders of the board are visible here:

![PCB Top](https://gitlab.com/swag-home/extension-devices/solar-power/-/raw/main/images/solar_top.PNG)
![PCB Bottom](https://gitlab.com/swag-home/extension-devices/solar-power/-/raw/main/images/solar_bottom.PNG)

## Building the board

Design file of the PCB and typons are available in the [pcb folder](https://gitlab.com/swag-home/extension-devices/solar-power/-/tree/main/pcb)
